output "public_ip" {
  value = google_compute_address.public_ephemeral_ip.address
}

output "lb_public_ip" {
  value = module.gce_lb_http.external_ip
}
