resource "google_project_service" "compute_service" {
  project = var.GCP_PROJECT_ID
  service = "compute.googleapis.com"
}

resource "google_compute_address" "public_ephemeral_ip" {
  name = "ubuntu-vm"
}

resource "google_compute_firewall" "allow_ssh" {
  name          = "allow-ssh"
  network       = google_compute_network.vpc_network.name
  target_tags   = ["allow-ssh"] // this targets our tagged VM
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

resource "google_compute_firewall" "allow_http" {
  name          = "allow-http"
  network       = google_compute_network.vpc_network.name
  target_tags   = ["allow-http"] // this targets our tagged VM
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
}

resource "google_compute_firewall" "allow_https" {
  name          = "allow-https"
  network       = google_compute_network.vpc_network.name
  target_tags   = ["allow-https"] // this targets our tagged VM
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }
}

resource "google_compute_instance" "vm_instance" {
  name         = "app-instance"
  machine_type = "e2-micro"

  desired_status = "RUNNING"
  # desired_status = "TERMINATED"

  tags = ["app-instance", "allow-ssh", "allow-http", "allow-https"]

  labels = {
    app                   = "sample"
    goog-ops-agent-policy = "v2-x86-template-1-1-0"
  }

  boot_disk {
    initialize_params {
      image = "ubuntu-2204-jammy-v20230919"
    }
  }

  network_interface {
    network    = google_compute_network.vpc_network.self_link
    subnetwork = google_compute_subnetwork.public_network.self_link

    access_config {
      // Ephemeral public IP
      nat_ip = google_compute_address.public_ephemeral_ip.address
    }
  }

  metadata = {
    ssh-keys        = "${local.ssh_user}:${file("~/.ssh/aws_rsa.pub")}"
    enable-osconfig = "TRUE"
  }
}

module "agent_policy" {
  source  = "terraform-google-modules/cloud-operations/google//modules/agent-policy"
  version = "~> 0.2.3"

  project_id = var.GCP_PROJECT_ID

  policy_id = "ops-agents-metric-and-logs-agents-policy"
  agent_rules = [
    {
      type               = "ops-agent"
      version            = "current-major"
      package_state      = "installed"
      enable_autoupgrade = true
    },
    {
      type               = "metrics"
      version            = "current-major"
      package_state      = "installed"
      enable_autoupgrade = true
    },
    {
      type               = "logging"
      version            = "current-major"
      package_state      = "installed"
      enable_autoupgrade = true
    },
  ]

  group_labels = [
    {
      app = "sample"
    }
  ]

  os_types = [
    {
      short_name = "ubuntu"
      version    = "2204"
    }
  ]
}

####
##
## Unmanagable Instance Group
##
####
resource "google_compute_instance_group" "vm_ig" {
  name = "vm-ig"

  instances = [
    google_compute_instance.vm_instance.id
  ]

  zone = var.GCP_ZONE
}
####
##
## /Unmanagable Instance Group
##
####

####
##
## Managable Instance Group
##
####

resource "google_compute_image" "vm_image" {
  name = "vm-image"

  # source_disk = google_compute_instance.vm_instance.self_link
  source_disk = "/projects/${var.GCP_PROJECT_ID}/zones/${var.GCP_ZONE}/disks/${google_compute_instance.vm_instance.name}"
}

resource "google_compute_instance_template" "vm_instance_template" {
  name = "vm-instance-template"

  machine_type = "e2-micro"

  tags = ["app-instance", "allow-ssh", "allow-http", "allow-https"]

  disk {
    source_image = google_compute_image.vm_image.self_link
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network    = google_compute_network.vpc_network.self_link
    subnetwork = google_compute_subnetwork.public_network.self_link

    # access_config {
    #   // Ephemeral public IP
    # }
  }

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  metadata = {
    ssh-keys        = "${local.ssh_user}:${file("~/.ssh/aws_rsa.pub")}"
    enable-osconfig = "TRUE"
  }
}


resource "google_compute_health_check" "vm_autohealing" {
  name                = "vm-autohealing-health-check"
  check_interval_sec  = 5
  timeout_sec         = 5
  healthy_threshold   = 2
  unhealthy_threshold = 10 # 50 seconds

  http_health_check {
    request_path = "/"
    port         = "80"
  }
}

resource "google_compute_target_pool" "vm_pool" {
  name = "vm-instance-pool"
}

resource "google_compute_instance_group_manager" "vm_igm" {
  name = "vm-igm"

  base_instance_name = "app"

  version {
    instance_template = google_compute_instance_template.vm_instance_template.self_link_unique
  }

  target_pools = [google_compute_target_pool.vm_pool.id]
  target_size  = 2

  auto_healing_policies {
    health_check      = google_compute_health_check.vm_autohealing.id
    initial_delay_sec = 300
  }
}

resource "google_compute_autoscaler" "vm_autoscaler" {
  provider = google-beta

  name   = "vm-autoscaler"
  target = google_compute_instance_group_manager.vm_igm.id

  autoscaling_policy {
    max_replicas    = 5
    min_replicas    = 2
    cooldown_period = 60

    cpu_utilization {
      target = 0.72
    }
  }
}

####
##
## /Managable Instance Group
##
####
