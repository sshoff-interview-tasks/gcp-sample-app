## Enable APIs

* Compute Engine API
* Service Networking API
* Cloud SQL Admin API

## Sensitive Data

Create a file in the directory `terraform` with the name `secrets.tfvars` and insert into:
```
POSTGRESQL_PASSWORD="<password>"
```

## Run
On Debian/Ubuntu:
```bash
$ sudo apt-get install apt-transport-https ca-certificates gnupg -y

$ echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

$ sudo curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

$ sudo apt-get update && sudo apt-get install google-cloud-sdk

$ gcloud auth application-default login
```

Go to the directory with terraform:
```bash
$ cd ./terraform
```

### Terraform
```bash
$ terraform init
$ terraform fmt
$ terraform validate
$ terraform apply -var-file="secrets.tfvars"
$ terraform destroy -var-file="secrets.tfvars"
```