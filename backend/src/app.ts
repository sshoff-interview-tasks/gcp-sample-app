import express from 'express';
import Db from './db';
import { hostname } from 'os';

const app = express();
const port = 3000;

const db = new Db();

app.get('/', async (req, res) => {
    const query = `
        SELECT *
        FROM ${db.tableName};
    `;

    const answer = await db.query(query);

    const message = `
        <h1>Hello, this is server with name ${hostname()}!</h1>
        <h2>Data from DB below:</h2>
        <p>${answer[0].field}</p>
    `;

    res.send(message);
});

app.listen(port, () => {
    return console.log(`Express is listening at http://localhost:${port}`);
});