variable "GCP_PROJECT_ID" {
  default = "shaped-network-401113"
}

variable "GCP_REGION" {
  default = "me-west1"
}

variable "GCP_ZONE" {
  default = "me-west1-a"
}

variable "POSTGRESQL_PASSWORD" {
  description = "Password for DB with name `app`"
  type        = string
  sensitive   = true
}

locals {
  ssh_user = "ubuntu"

  postgresql = {
    user = "app_user"
    db   = "app_db"
  }
}