module "gce_lb_http" {
  source  = "GoogleCloudPlatform/lb-http/google"
  version = "~> 9.0"

  name    = "http-load-balanser"
  project = var.GCP_PROJECT_ID

  backends = {
    default = {
      protocol    = "HTTP"
      port        = 80
      port_name   = "http"
      timeout_sec = 10
      enable_cdn  = false

      health_check = {
        request_path = "/"
        port         = 80
      }

      log_config = {
        enable      = true
        sample_rate = 1.0
      }

      groups = [
        # {
        #   group = google_compute_instance_group.vm_ig.self_link
        # },
        {
          group                 = google_compute_instance_group_manager.vm_igm.instance_group
          balancing_mode        = "RATE"
          capacity_scaler       = 1.0
          max_rate_per_instance = 500
        }
      ]

      iap_config = {
        enable               = false
        oauth2_client_id     = null
        oauth2_client_secret = null
      }
    }
  }
}
