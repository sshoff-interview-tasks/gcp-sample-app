resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "master" {
  name             = "master-${random_id.db_name_suffix.hex}"
  region           = var.GCP_REGION
  database_version = "POSTGRES_15"

  depends_on = [google_service_networking_connection.private_access_connection]

  settings {
    tier              = "db-f1-micro"
    availability_type = "REGIONAL"
    disk_size         = "10"

    backup_configuration {
      enabled = true
    }

    ip_configuration {
      ipv4_enabled                                  = false
      private_network                               = google_compute_network.vpc_network.id
      enable_private_path_for_google_cloud_services = true
    }

    location_preference {
      zone = var.GCP_ZONE
    }
  }

  deletion_protection = false
}

resource "google_sql_user" "main" {
  depends_on = [
    google_sql_database_instance.master
  ]
  name     = local.postgresql.user
  instance = google_sql_database_instance.master.name
  password = var.POSTGRESQL_PASSWORD
}

resource "google_sql_database" "main" {
  depends_on = [
    google_sql_user.main
  ]
  name     = local.postgresql.db
  instance = google_sql_database_instance.master.name
}

resource "google_sql_database_instance" "read_replica" {
  name = "replica-${random_id.db_name_suffix.hex}"
  # master_instance_name = "${var.GCP_PROJECT_ID}:${google_sql_database_instance.master.name}"
  master_instance_name = google_sql_database_instance.master.name
  region               = var.GCP_REGION
  database_version     = "POSTGRES_15"

  depends_on = [google_service_networking_connection.private_access_connection]

  replica_configuration {
    failover_target = false
  }

  settings {
    tier              = "db-f1-micro"
    availability_type = "ZONAL"
    disk_size         = "10"

    backup_configuration {
      enabled = false
    }

    ip_configuration {
      # ipv4_enabled    = true
      # private_network = google_compute_network.vpc_network.id
      ipv4_enabled                                  = false
      private_network                               = google_compute_network.vpc_network.id
      enable_private_path_for_google_cloud_services = true
    }

    location_preference {
      zone = var.GCP_ZONE
    }
  }

  deletion_protection = false
}
