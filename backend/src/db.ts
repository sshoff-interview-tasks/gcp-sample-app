import { Client } from 'pg';
import * as dotenv from 'dotenv';

dotenv.config();

export default class Db {
    public tableName = "data";

    private client;

    constructor(){
        this.client = new Client({
            host: process.env.DATABASE_HOST,
            port: +process.env.DATABASE_PORT,
            user: process.env.DATABASE_USER,
            password: '' + process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_DB,
        });
        this.client.connect();

        this.initDb();
    }

    public async query(query){
        try {
            const answer = await this.client.query(query);

            return answer.rows;
        } catch (err) {
            console.log(err.stack);
        }
    }

    private async initDb() {
        const checkResult = await this.checkIfDbExist();

        if (!checkResult) {
            
            const queryCreateTable = `
                CREATE TABLE "${this.tableName}" (
                    "field" varchar
                );
            `;

            const queryInsertRow = `
                INSERT INTO "${this.tableName}"
                VALUES
                    ('String in Data Base');
            `;
    
            try {
                const answ = await this.client.query(queryCreateTable);
                console.log(answ);
                await this.client.query(queryInsertRow);
            } catch (err) {
                console.log(err);
            }
        }
    }

    private async checkIfDbExist() {
        const query = `
            SELECT 
                COUNT(table_name)
            FROM 
                information_schema.tables 
            WHERE 
                table_schema LIKE 'public' AND 
                table_type LIKE 'BASE TABLE' AND
                table_name = '${this.tableName}';
        `;
    
        try {
            const res = await this.client.query(query);

            if (res.rows[0].count == 1) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            console.log(err);
        }
    }
}
