provider "google" {
  project = var.GCP_PROJECT_ID
  region  = var.GCP_REGION
  zone    = var.GCP_ZONE
}

provider "google-beta" {
  project = var.GCP_PROJECT_ID
  region  = var.GCP_REGION
  zone    = var.GCP_ZONE
}

terraform {
  required_providers {
    google = {
      version = "~> 4.84.0"
    }
    google-beta = {
      version = "~> 4.84.0"
    }
  }
  backend "gcs" {
    bucket = "gsp-sample-app-terraform-bucket"
    prefix = "terraform/state"
  }
}
